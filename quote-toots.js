(function(callback) {
    if (document.readyState !== 'loading') {
        callback()
    } else {
        document.addEventListener('DOMContentLoaded', callback, {
            once: true
        })
    }
})(async function () {
    console.log('INIT PLUGIN')
    if (!isMastodon()) {
        return
    }

    let feed = await initForFeed()
    const feedColumn = document.querySelector('.columns-area__panels__main > .columns-area')

    const observer = new MutationObserver(async function () {
        if (!feed) {
            return
        }

        feed = null
        feed = await initForFeed()
    })

    observer.observe(feedColumn, {
        childList: true,
        subtree: false
    })

    window.addEventListener('message', function (ev) {
        if (ev.data.type === 'setHeight' && ev.data.id.startsWith('qt-')) {
            const id = ev.data.id
            const height = ev.data.height

            const iframe = document.querySelector(`iframe.mastodon-embed[data-id="${id}"]`)
            if (iframe) {
                iframe.height = height
            }
        }
    })

})

async function initForFeed () {
    const feed = await waitForSelector('[role="feed"]')
    console.log('INIT FOR FEED', feed)
    const observer = new MutationObserver(function (mutations) {
        for (const mutation of mutations) {
            initForToots(mutation.addedNodes)
        }
    })

    observer.observe(feed, {
        childList: true,
        subtree: false
    })

    initForToots(feed.childNodes)

    return feed
}

function initForToots (childNodes) {
    for (const childNode of childNodes) {
        if (childNode.nodeType !== Node.ELEMENT_NODE) {
            continue
        }
        if (childNode.nodeName !== 'ARTICLE') {
            continue
        }
        if (!childNode.dataset.id) {
            continue
        }

        initForToot(childNode)
    }
}

let autoIncrementId = 1
function getNextId() {
    return 'qt-'  + autoIncrementId++
}

function initForToot (articleNode) {
    if (articleNode.dataset.qt) {
        return
    }

    const intersectionObserver = new IntersectionObserver(function (entries) {
        if (!entries[0].isIntersecting) {
            return
        }

        const statusLinks = articleNode.querySelectorAll('a.status-link')
        for (const statusLink of statusLinks) {
            setTimeout(initEmbed, 0, statusLink, articleNode)
        }

        setTimeout(initQuoteTootButton, 0, articleNode)
    }, {
        root: null,
        threshold: 1
    })

    intersectionObserver.observe(articleNode)
    articleNode.dataset.qt = 1
}

function initEmbed (statusLink, articleNode) {
    const statusUrl = statusLink.href
    if (!matchMastodonUrl(statusUrl)) {
        return
    }

    if (statusLink.dataset.qt) {
        return
    }

    const id = getNextId()
    const embedIframe = document.createElement('iframe')
    // TODO: Is there an API to get the URL?
    embedIframe.src = statusUrl + '/embed'
    embedIframe.classList.add('mastodon-embed')
    embedIframe.classList.add('status-card')

    embedIframe.style.width = '100%'
    embedIframe.style.maxWidth = '100%'
    embedIframe.allowFullscreen = true
    embedIframe.dataset.id = id

    embedIframe.addEventListener('load', function() {
        embedIframe.contentWindow.postMessage({
            type: 'setHeight',
            id: id,
        }, '*')
    }, { once: true })

    const actionBar = articleNode.querySelector('.status__action-bar')
    actionBar.parentNode.insertBefore(embedIframe, actionBar)

    const statusCard = articleNode.querySelector(`a.status-card[href="${statusUrl}"]`)
    if (statusCard) {
        statusCard.parentNode.removeChild(statusCard)
    }

    statusLink.dataset.qt = 1
    // statusCard.innerHTML = ''
    // statusCard.classList.remove('compact')
    // statusCard.appendChild(embedIframe)
}

function initQuoteTootButton (articleNode) {
    const dropdownButton = articleNode.querySelector('.status__action-bar__dropdown button')
    if (!dropdownButton) {
        return
    }

    dropdownButton.addEventListener('click', function () {
        setTimeout(function () {
            const dropdownMenu = document.querySelector('.dropdown-menu')
            if (!dropdownMenu) {
                return
            }

            const list = dropdownMenu.querySelector('.dropdown-menu__container__list')
            if (!list) {
                return
            }

            const tootLink = list.querySelector('a:not([href="#"])')
            if (!tootLink) {
                return
            }
            if (!matchMastodonUrl(tootLink.href)) {
                return
            }

            const quoteTootItem = document.createElement('li')
            quoteTootItem.classList.add('dropdown-menu__item')

            const quoteTootButton = document.createElement('a')
            quoteTootButton.href = '#'
            quoteTootButton.role = 'button'
            quoteTootButton.tabindex = '0'
            quoteTootButton.textContent = 'Quote Toot'

            quoteTootButton.addEventListener('click', function (ev) {
                ev.preventDefault()

                const composeTextarea = document.querySelector('.compose-form textarea')
                if (!composeTextarea) {
                    return
                }

                composeTextarea.dispatchEvent(new MouseEvent('click', {
                    bubbles: true
                }))

                composeTextarea.focus()
                composeTextarea.value = '\n\n' + tootLink.href
                composeTextarea.dispatchEvent(new InputEvent('input'))
                composeTextarea.setSelectionRange(0, 0)
            })

            quoteTootItem.appendChild(quoteTootButton)
            list.insertBefore(quoteTootItem, list.children[3])
        }, 0)
    })
}

function isMastodon() {
    return document.getElementById('mastodon') !== null
}

const mastodonUrlRe = /^https?:\/\/([^\/]+)\/@([^\/]+)\/(\d+)$/
function matchMastodonUrl (url) {
    const match = mastodonUrlRe.exec(url)
    if (!match) {
        return null
    }

    return {
        domain: match[1],
        handle: match[2],
        id: match[3]
    }
}

async function waitForSelector(selector) {
    const el = document.querySelector(selector)
    if (el) {
        return el
    }

    await delay(100)
    return waitForSelector(selector)
}

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}
